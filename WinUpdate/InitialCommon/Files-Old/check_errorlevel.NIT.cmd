@echo off
rem *******************************************************
rem This Script only Test Errorlevel of a Script File
rem PARAMETERS: Script
rem RETURN:	0 if Success
rem		1 if Filesystem Error
rem		2 bitsadmin not found
rem		3 curl not found
rem		4 wget not found
rem		5 Unknown OS
rem		6 Wrong Server
rem *******************************************************
@echo off
set Script=%1
if not exist "%Script%" goto End
call %1
if errorlevel 6 echo "Wrong Server" && goto End
if errorlevel 5 echo "Unknown OS" && goto End
if errorlevel 4 echo "Wget not founnd" && goto End
if errorlevel 3 echo "Curl not found" && goto End
if errorlevel 2 echo "Bitsadmin not found" && goto End
if errorlevel 1 echo "File System Error" && goto End
if errorlevel 0 echo "Success" && goto End

:End
echo Script Terminated
exit /b 0