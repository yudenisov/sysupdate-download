@echo off
rem ***************************************************************************
rem
rem NIT.getVersion.bat
rem
rem This Script Define the Version of OS Windows and its Architecture
rem and Download Corresponding Scripts
rem
rem PARAMETERS:	None
rem RETURNS:	0 if Success
rem		1 if FileSystem Error or Missig System Files
rem		2 if Unknown OS
rem
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

rem set PRODUCT_NAME=TEMPLATE
SET FIRM_NAME=NIT
set BASE_FOLDER=/WinUpdate/InitialCommon/

rem
rem Set Directories Path
set curdirforurl=%CD%
set UTIL=c:\Util
set DEST_DIR=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set TEMPPUB=c:\pub1\Distrib

rem Check if FileSystem Correct
echo Check AllUserProfile
if not exist "%ALLUSERSPROFILE%" exit /b 1

rem
rem Set Directories Path

rem Initialization Download Variables

set http_pref001=http
set http_host001=file.tuneserv.ru
set http_port001=80
set http_dir0001=/WinUpdate/

rem Check File System Integrity
echo Check File System Integrity
if not exist "%SystemRoot%\system32\find.exe" exit /b 1
if not exist "%TEMP%" exit /b 1
if not exist "%UTIL%" exit /b 1
if not exist "%DEST_DIR%" exit /b 1
if not exist "%PUB1%" exit /b 1
if not exist "%TEMPPUB%" exit /b 1
if not exist "%SystemRoot%\System32\wbem\WMIC.EXE" exit /b 1
if not exist "%SystemRoot%\System32\reg.exe" exit /b 1
if not exist "%SystemRoot%\System32\wscript.exe" exit /b 1
if not exist "%SystemRoot%\System32\cscript.exe" exit /b 1
if not exist "%SystemRoot%\System32\shutdown.exe" exit /b 1
if not exist "%SystemRoot%\System32\netsh.exe" exit /b 1
if not exist "%SystemRoot%\System32\attrib.exe" exit /b 1
rem if not exist "%UTIL%\nit.http.wise.wsf" exit /b 1
rem if not exist "%SystemRoot%\System32\bitsadmin.exe" exit /b 1
if not exist "%TEMP%\nit.http.wise.wsf" exit /b 1
rem if not exist "%UTIL%\nit.http.wise.wsf" exit /b 1

rem Copy to %UTIL%
echo Copy to Util
del /Q /F "%UTIL%\nit.http.wise.wsf"
copy "%TEMP%\nit.http.wise.wsf" "%UTIL%\nit.http.wise.wsf"
if not exist "%UTIL%\nit.http.wise.wsf" exit /b 1

rem Derivatives Variables
set hostbase=%http_pref001%://%http_host001%:%http_port001%%BASE_FOLDER%
set hostproduct=%http_pref001%://%http_host001%:%http_port001%%PROD_FOLDER%

rem set Exequtable Files Variables
set LOADNITSYSUPDATE=Load-NIT-SysUpdate.bat
set LOADSUDL=NIT-SUDl-00.cmd

rem Delete old Variables, Files and Rules
del /F /Q "%TEMP%\%LOADNITSYSUPDATE%"
del /F /Q "%TEMP%\%LOADSUDL%"

rem Set OS Architecture
Set xOS=x64 & If "%PROCESSOR_ARCHITECTURE%"=="x86" If Not Defined PROCESSOR_ARCHITEW6432 Set xOS=x32


ver | %SystemRoot%\system32\find.exe "Windows [Version 10" > nul
if not errorlevel 1 goto win_10
ver | %SystemRoot%\system32\find.exe "Windows [Version 6.3" > nul
if not errorlevel 1 goto win_8_1
ver | %SystemRoot%\system32\find.exe "Windows [Version 6.2" > nul
if not errorlevel 1 goto win_8
ver | %SystemRoot%\system32\find.exe "Windows [Version 6.1" > nul
if not errorlevel 1 goto win_7
ver | %SystemRoot%\system32\find.exe "Windows [Version 6.0" > nul
if not errorlevel 1 goto win_vista
ver | %SystemRoot%\system32\find.exe "Windows XP" > nul
if not errorlevel 1 goto win_XP
ver | %SystemRoot%\system32\find.exe "Windows [Version 5.2" > nul
if not errorlevel 1 goto win_2003
ver | %SystemRoot%\system32\find.exe "Windows [����� 5.2" > nul
if not errorlevel 1 goto win_2003
ver | %SystemRoot%\system32\find.exe "Windows [Version 5.0" > nul
if not errorlevel 1 goto win_2000
ver | %SystemRoot%\system32\find.exe "Windows [����� 5.0" > nul
if not errorlevel 1 goto win_2000
ver | %SystemRoot%\system32\find.exe "win" > nul
if not errorlevel 1 goto win_98
echo System UnSupported
goto end

:win_10

set PRODUCT_NAME=Win10
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

rem Host Folders Definition
rem
set PROD_FOLDER=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set hostproduct=%http_pref001%://%http_host001%:%http_port001%%PROD_FOLDER%

rem Download Programs & Components
rem
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" wget.exe %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" CURL.EXE %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" LIBCURL.DLL %hostproduct% %Util%
rem "%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" %LOADNITSYSUPDATE% %hostbase%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" %LOADSUDL% %hostproduct%
rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%wget.exe "%Util%\wget.exe" %hostproduct%/LIBCURL.DLL "%Util%\libcurl.dll" %hostproduct%/CURL.EXE "%Util%\curl.exe"

rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostbase%%LOADNITSYSUPDATE% "%TEMP%\%LOADNITSYSUPDATE%"
goto end

:win_8_1

set PRODUCT_NAME=Win8.1
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

if %PRODUCT_NAME_FOLDER%==Win8.1x86 goto Unknown

rem Host Folders Definition
rem
set PROD_FOLDER=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set hostproduct=%http_pref001%://%http_host001%:%http_port001%%PROD_FOLDER%

rem Download Programs & Components
rem
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" wget.exe %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" CURL.EXE %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" LIBCURL.DLL %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" %LOADSUDL% %hostproduct%
rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%wget.exe "%Util%\wget.exe" %hostproduct%/LIBCURL.DLL "%Util%\libcurl.dll" %hostproduct%/CURL.EXE "%Util%\curl.exe"

rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%%LOADSUDL% "%TEMP%\%LOADSUDL%"
goto end

:win_8

set PRODUCT_NAME=Win8
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

goto Unknown

:win_7

set PRODUCT_NAME=Win7
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

rem Host Folders Definition
rem
set PROD_FOLDER=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set hostproduct=%http_pref001%://%http_host001%:%http_port001%%PROD_FOLDER%

rem Download Programs & Components
rem
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" wget.exe %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" CURL.EXE %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" LIBCURL.DLL %hostproduct% %Util%
rem "%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" %LOADNITSYSUPDATE% %hostbase%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" %LOADSUDL% %hostproduct%

rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%wget.exe "%Util%\wget.exe" %hostproduct%/LIBCURL.DLL "%Util%\libcurl.dll" %hostproduct%/CURL.EXE "%Util%\curl.exe"

rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostbase%%LOADNITSYSUPDATE% "%TEMP%\%LOADNITSYSUPDATE%"
goto end

:win_vista

set PRODUCT_NAME=WinVista
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

goto Unknown

:win_2000

set PRODUCT_NAME=Win2k
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

goto Unknown

:win_XP

set PRODUCT_NAME=WinXP
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

if %PRODUCT_NAME_FOLDER%==WinXPx64 goto Unknown

rem Host Folders Definition
rem
set PROD_FOLDER=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set hostproduct=%http_pref001%://%http_host001%:%http_port001%%PROD_FOLDER%

rem Download Programs & Components
rem
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" wget.exe %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" CURL.EXE %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" LIBCURL.DLL %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" %LOADSUDL% %hostproduct%
rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%wget.exe "%Util%\wget.exe" %hostproduct%/LIBCURL.DLL "%Util%\libcurl.dll" %hostproduct%/CURL.EXE "%Util%\curl.exe"

rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%%LOADSUDL% "%TEMP%\%LOADSUDL%"
goto end

:win_2003

set PRODUCT_NAME=Win2k3
if %xOS%==x64 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x64
if %xOS%==x32 set PRODUCT_NAME_FOLDER=%PRODUCT_NAME%x86

if %PRODUCT_NAME_FOLDER%==Win2k3x64 goto Unknown

rem Host Folders Definition
rem
set PROD_FOLDER=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set hostproduct=%http_pref001%://%http_host001%:%http_port001%%PROD_FOLDER%

rem Download Programs & Components
rem
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" wget.exe %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" CURL.EXE %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" LIBCURL.DLL %hostproduct% %Util%
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" %LOADSUDL% %hostproduct%
rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%wget.exe "%Util%\wget.exe" %hostproduct%/LIBCURL.DLL "%Util%\libcurl.dll" %hostproduct%/CURL.EXE "%Util%\curl.exe"

rem "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %hostproduct%%LOADSUDL% "%TEMP%\%LOADSUDL%"
goto end

:win98

goto Unknown

:Unknown
exit /b 2

:end
echo %PRODUCT_NAME_FOLDER%
echo %hostproduct%wget.exe
exit /b 0