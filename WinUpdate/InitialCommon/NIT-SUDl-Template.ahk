; *******************************************************
; 
; NIT-SUDl-Template.ahk
; 
; This Templater file Downloads and Installs NIT
; Program Products and Plugins
; on local computer
; 
; ALGORITHM
; 
; PARAMETERS: NO
; RETURN:	0 if Success
; 		1 if Filesystem Error
; 		2 bitsadmin not found
; 		3 curl not found
; 		4 wget not found
; 		5 Unknown OS
; 		6 Wrong Server
; 
; *******************************************************

; Initialization of Variables
; Metadata

PRODUCT_NAME=TEMPLATE
FIRM_NAME=NIT

; 
; Set Directories Path
curdirforurl=%CD%
UTIL=c:\Util
DEST_DIR=C:\NIT.SYSUPDATE
PUB1=C:\pub1
TEMPPUB=c:\pub1\Distrib

; Check if FileSystem Correct
IfNotExist, %ALLUSERSPROFILE% 
{
	MsgBox, 16, "System Error", Error!`n ALLUSERSPROFILE`t%ALLUSERSPROFILE%  is not Exist!, 5
	Exit,  1
}
ifNotExist, %SystemRoot%
{
	MsgBox, 16, "System Error", Error!`n SystemRoot`t%SystemRoot%  is not Exist!, 5
	Exit,  1
}

; Set EXE Variables
;
FINDEXE = %SystemRoot%\system32\find.exe
WMICEXE = %SystemRoot%\System32\wbem\WMIC.EXE
NITWISEWSF = %UTIL%\nit.http.wise.wsf
;NITWISEWSF = %TEMP%\nit.http.wise.wsf
BITSADMINEXE = %SystemRoot%\System32\bitsadmin.exe
POWERSHELLEXE = %SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe
REGEXE = %SystemRoot%\System32\reg.exe
WSCRIPTEXE = %SystemRoot%\System32\wscript.exe
CSCRIPTEXE = %SystemRoot%\System32\cscript.exe
SHUTDOWNEXE = %SystemRoot%\System32\shutdown.exe
NETSHEXE = %SystemRoot%\System32\netsh.exe
ATTRIBEXE = %SystemRoot%\System32\attrib.exe 

; Check if Utilites Presents
IfNotExist, %FINDEXE%
{
	MsgBox, 16, "System Error", Error!`nFINDEXE`t%FINDEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %BITSADMINEXE%
{
	MsgBox, 16, "System Error", Error!`nBITSADMINEXE`t%BITSADMINEXE%  is not Exist!, 5
	Exit,  2
}
IfNotExist,  %WMICEXE%
{
	MsgBox, 16, "System Error", Error!`nWMICEXE`t%WMICEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %NITWISEWSF%
{
	MsgBox, 16, "System Error", Error!`nNITWISEWSF`t%NITWISEWSF%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %REGEXE%
{
	MsgBox, 16, "System Error", Error!`nREGEXE`t%REGEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %WSCRIPTEXE%
{
	MsgBox, 16, "System Error", Error!`nWSCRIPTEXE`t%WSCRIPTEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %CSCRIPTEXE%
{
	MsgBox, 16, "System Error", Error!`nCSCRIPTEXE`t%CSCRIPTEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %SHUTDOWNEXE%
{
	MsgBox, 16, "System Error", Error!`nSHUTDOWSNEXE`t%SHUTDOWNEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %NETSHEXE%
{
	MsgBox, 16, "System Error", Error!`nNETSHEXE`t%NETSHEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %ATTRIBEXE%
{
	MsgBox, 16, "System Error", Error!`nATTRIBEXE`t%ATTRIBEXE%  is not Exist!, 5
	Exit,  1
}
;IfNotExist,  %NITWISEEXE%
;{
;	MsgBox, 16, "System Error", Error!`nNITWISEEXE`t%NITWISEEXE%  is not Exist!, 5
;	Exit,  1
;}
IfNotExist,  %POWERSHELLEXE%
{
	MsgBox, 16, "System Error", Error!`nPOWERSHELLEXE`t%POWERSHELLEXE%  is not Exist!, 5
	Exit,  1
}
IfNotExist,  %COMSPEC%
{
	MsgBox, 16, "System Error", Error!`nCOMSPEC`t%COMSPEC%  is not Exist!, 5
	Exit,  1
}
IfNotExist, %TEMP% 
{
	MsgBox, 16, "System Error", Error!`nTEMP`t%TEMP%  is not Exist!, 5
	Exit,  1
}

; Check If System Product Directory Exists
; 
PRODUCTNAMEDIR=%ALLUSERSPROFILE%\%FIRM_NAME%\%PRODUCT_NAME%
; IfNotExist, %PRODUCTNAMEDIR% Exit,  1
IfNotExist, %UTIL% 
{
	MsgBox, 16, "System Error", Error!`nUTIL`t%UTIL%  is not Exist!, 5
	Exit,  1
}
IfNotExist, %DEST_DIR% 
{
	MsgBox, 16, "System Error", Error!`nDEST_DIR`t%DEST_DIR%  is not Exist!, 5
	Exit,  1
}
IfNotExist, %PUB1% 
{
	MsgBox, 16, "System Error", Error!`nPUB1`t%PUB1%  is not Exist!, 5
	Exit,  1
}
IfNotExist, %TEMPPUB% 
{
	MsgBox, 16, "System Error", Error!`nTEMPPUB`t%TEMPPUB%  is not Exist!, 5
	Exit,  1
}
;IfNotExist, %PRODUCTNAMEDIR% 
;{
;	MsgBox, 16, "System Error", Error!`n %PRODUCTNAMEDIR%  is not Exist!, 5
;	Exit,  1
;}

; Initialization Download Variables
; 
http_pref001=http
http_host001=file.tuneserv.ru
http_port001=80
http_dir0001=/WinUpdate/
http_dir0000=/Exponenta/
http_echodir=/WinUpdate/InitialCommon/

; set Folders
; 
BASE_FOLD=%http_echodir%

; set Executable Files Variables
LOADNITSYSUPDATE=Load-NIT-SysUpdate.bat
LOADSUDL=NIT-SUDl-00.cmd
GETVALIDVERSION=NIT.getVersion.bat

; Derivatives Variables Initial
host0=%http_pref001%://%http_host001%:%http_port001%%BASE_FOLD%
LocalFolder=%TEMP%

; Download and Execute
; "%SystemRoot%\System32\bitsadmin.exe" /Transfer STEA_TRANSFER /DOWNLOAD /Priority FOREGROUND %host0%%GETVALIDVERSION% "%LocalFolder%\%GETVALIDVERSION%"
;"%SystemRoot%\System32\cscript.exe" "%TEMP%\nit.http.wise.wsf" %GETVALIDVERSION% %host0% "%LocalFolder%"
;IfNotExist, %LocalFolder%\ %GETVALIDVERSION%
;{
;	Exit,  6
;}
;call "%LocalFolder%\%GETVALIDVERSION%"
; Check Error
;if %ERRORLEVEL%==2 goto Unknown
;if %ERRORLEVEL%==1 Exit,  1

; set CURL & WGET Variables
; 
CURLEXE = %UTIL%\CURL.EXE
WGETEXE = %UTIL%\wget.exe
IfNotExist, %CURLEXE%
{
	MsgBox, 16, "System Error", Error!`nCURLEXE`t%CURLEXE%  is not Exist!, 5
	Exit,  3
}
IfNotExist, %WGETEXE%
{
	MsgBox, 16, "System Error", Error!`nWGETEXE`t%WGETEXE%  is not Exist!, 5
	Exit,  4
}

; Enable all protocols for curl, wget
;%SystemRoot%\System32\netsh.exe advfirewall firewall delete rule name="Download Application Rule"

;%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="Download Application Rule" dir=in action=allow program="%WGETEXE%" enable=yes
;%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="Download Application Rule" dir=out action=allow program="%WGETEXE%" enable=yes
;%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="Download Application Rule" dir=in action=allow program="%CURLEXE%" enable=yes
;%SystemRoot%\System32\netsh.exe advfirewall firewall add rule name="Download Application Rule" dir=out action=allow program="%CURLEXE%" enable=yes

; Derivatives Variables
hostecho = %http_pref001%://%http_host001%:%http_port001%%http_echodir%
host0 = %http_pref001%://%http_host001%:%http_port001%.%BASE_FOLD%
LocalFolder=%TEMP%

; set Path Variables
CUR_FOLD = /WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
NEXT_FOLD = /WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/

; TEST Download and Execute echo.bat
; 
ECHO = echo.bat
Run %CURLEXE% %hostecho%%ECHO% -o %LocalFolder%\%ECHO%, , min
IfNotExist, %LocalFolder%\%ECHO% 
{
	MsgBox, 0, , Host = %hostecho%%ECHO%, 5
	MsgBox, %LocalFolder%\%ECHO%  not Exist!
	Exit,  6
}
; call "%LocalFolder%\%ECHO%" 

; End Test

; Execute Downloaded System Update Scripts
goto End

NonSU:
goto End

Unknown:
; Unknown OS
MsgBox, Run at Unknown OS
Exit,  5

; The End of the Script
End:
MsgBox, "Success Update Script Start"
Exit,  0
