@echo off
rem *******************************************************
rem NIT-SU.cmd
rem This Script Checks Integrity of Computer Filesystem
rem and Install Necessary Modules
rem FOR WINDOWS 10X64
rem
rem PARAMETERS:	NONE
rem RETURNS:	NONE
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=Win10
set PRODUCT_NAME_FOLDER=Win10x64
SET FIRM_NAME=NIT
set OS_ARCH=x64

rem set ProductDir
set PRODUCTPROFILE=%ALLUSERSPROFILE%\%FIRM_NAME%\%PRODUCT_NAME%
md "%PRODUCTPROFILE%"

rem Check if NIT.SYSUPDATE Directory Exists
set NITSYS=C:\NIT.SYSUPDATE
set LocalFolder=%NITSYS%
if not exist "%LocalFolder%" goto EndError

rem Check Filesystem Integrity
echo Check Filesystem Integrity...
set CHCMD=NIT-Integrity.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
if errorlevel 3 goto EndError
if errorlevel 2 goto EndError
if errorlevel 1 goto EndError

rem Install NET Framework
echo Install NET Framework...
rem set CHCMD=NIT-SUDl-Framework.cmd
rem if not exist "%LocalFolder%\%CHCMD%" goto EndError
rem call "%LocalFolder%\%CHCMD%"
rem if errorlevel 6 goto EndError
rem if errorlevel 1 goto EndError

rem Install Other
echo Install Other...
set CHCMD=NIT-SUDl-Other.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
rem if errorlevel 6 goto EndError
rem if errorlevel 1 goto EndError

goto End

:EndError
echo Error Runing Script

rem End Of Script
:End
echo The End of the Script
 