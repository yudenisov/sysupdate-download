@echo on
rem *******************************************************
rem chock-install-001.cmd
rem This Script Installs the Chocolatey Packets without
rem Dependences (but with depended Packets) from Internet
rem
rem PARAMETERS:	NONE
rem RETURN:	NONE
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=TEMPLATE
set FIRM_NAME=NIT

rem
rem Set Directories Path
set PATHCMD=%SystemRoot%\System32
set PATHCMDWOW=%SystemRoot%\SysWOW64
set UTIL=c:\Util
set NITSYS=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set AdminT=C:\Elevation
set TEMPPUB=c:\pub1\Distrib
set curdirforurl=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE


rem Create target Directory
md C:\pub1\Distrib

rem Initialization Download Variables
rem
set http_pref001=http
set http_host001=file.tuneserv.ru
set http_port001=80
set http_dir0001=/WinUpdate/
set http_dir0000=/Exponenta/
set http_dir0003=/WinUpdate/WindowsMainUpdate/Other/
set http_echodir=/WinUpdate/InitialCommon/

set key=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce
set key1="HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell"

rem set CURL & WGET Variables
rem
echo Set CURL and WGET Variadles
set CURLEXE=%UTIL%\CURL.EXE
set WGETEXE=%UTIL%\wget.exe

rem Derivatives Variables
set hostecho=%http_pref001%://%http_host001%:%http_port001%%http_echodir%
set LocalFolder=%TEMPPUB%

rem set Path Variables

rem TEST Download and Execute echo.bat
rem
set ECHO=echo.bat
rem "%CURLEXE%" %hostecho%%ECHO% -o "%LocalFolder%\%ECHO%"
rem if not exist "%LocalFolder%\%ECHO%" echo %LocalFolder%\%ECHO% not exist && exit /b 6
rem call "%LocalFolder%\%ECHO%"

rem End Test

rem Download and Execute Payloads
rem 

rem Set ExecutionPolicy
%SystemRoot%\System32\reg.exe add %key1% /v ExecutionPolicy /t REG_SZ /d Unrestricted /f

rem This file installs the Chocolatey Packet Manager for Windows

set PowerShellDir=%SystemRoot%\System32\WindowsPowerShell
set ChocolateyBin=%ALLUSERSPROFILE%\chocolatey\bin
set BoxStarterBin=%ALLUSERSPROFILE%\Boxstarter

rem Check if PowerShell is Installed
if not exist %PowerShellDir% goto ps_not_installed
echo "Install Chocolatey..."
rem "%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
@%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
echo "Success! Chocolatey Packet Manager has been installed."
goto boxstart_install:

:boxstart_install
rem Check if Chocolatey is Installed
if not exist "%ChocolateyBin%" goto ch_not_installed
call "%ChocolateyBin%\RefreshEnv.cmd"
if exist "%BoxStarterBin%" goto bs_installed
echo "Install BoxStarter Shell..."
"%ChocolateyBin%\cinst.exe" -y boxstarter
call "%ChocolateyBin%\RefreshEnv.cmd"
:bs_installed
echo "Success! BoxStarter Shell has been installed."
goto boxstart_packets

:boxstart_packets
rem Check if BoxStarter is Installed
if not exist "%BoxStarterBin%" goto bx_not_installed
call "%ChocolateyBin%\RefreshEnv.cmd"
echo "Install BoxStarter Packets..."

rem Nothing ToDo

rem Install CURL and Wget
"%ChocolateyBin%\cinst.exe" -y curl
"%ChocolateyBin%\cinst.exe" -y wget

call "%ChocolateyBin%\RefreshEnv.cmd"
echo "Success! BoxStarter Shell has been installed."
goto End

rem End Payloads

:bx_not_installed
echo Error: BoxStarter packet is not installed
echo BoxStarter's Packets can't be installed
goto End

:ch_not_installed
echo Error: Chocolatey packet is not installed
echo BoxStarter Shell can't be installed
goto End

:ps_not_installed
echo Error: PowerShell packet is not installed
echo Chocolatey packet can't be installed
goto End

rem The End of the Script
:End
echo The End of %0 Script
