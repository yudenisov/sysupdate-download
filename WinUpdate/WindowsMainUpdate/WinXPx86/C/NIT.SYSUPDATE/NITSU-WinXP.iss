[Setup]
AppName=NITSU-WinXP
AppVersion=1.0.0.0
AppCopyright=Copyright (c) New Internet Technology Inc.
DefaultDirName=C:\NIT.SYSUPDATE\
VersionInfoVersion=1.0.0.0
VersionInfoCompany=New Internet Tecnologies Inc.
VersionInfoCopyright=Copyright (c) New Internet Technology Inc.
SolidCompression=True
MinVersion=0,5.01sp3
OnlyBelowVersion=0,5.02
LanguageDetectionMethod=locale
VersionInfoProductName=NIT System Update fir Windows XP
VersionInfoProductVersion=1.0.0.0
OutputBaseFilename=SetupNITSU-WinXP
AlwaysRestart=False
ArchitecturesAllowed=x86
SetupIconFile=.\NIT-Gear-logo.ico

[Files]
Source: "NIT-SU.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-Integrity.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-Framework.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-WinMF.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-IE7.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-IE8.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-SecUpdate01.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-Other.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "av-exclusions.html"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "nit.http.wise.wsf"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SU.wsf"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace

[Registry]
Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "NIT-SU"; ValueData: "C:\WINDOWS\System32\wscript.exe //NoLogo C:\NIT.SYSUPDATE\NIT-SU.wsf"; Flags: createvalueifdoesntexist uninsdeletevalue

