@echo on
rem *******************************************************
rem
rem DownloadAndExecuteTemplate-001.cmd
rem
rem This Templater file Downloads and Installs NIT
rem Program Products and Plugins
rem on local computer
rem
rem ALGORITHM
rem
rem PARAMETERS: NO
rem RETURN:	0 if Success
rem		1 if Filesystem Error
rem		2 bitsadmin not found
rem		3 curl not found
rem		4 wget not found
rem		5 Unknown OS
rem		6 Wrong Server
rem
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=WinXP
set PRODUCT_NAME_FOLDER=WinXPx86
SET FIRM_NAME=NIT

rem
rem Set Directories Path
set curdirforurl=%CD%
set UTIL=c:\Util
set DEST_DIR=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set TEMPPUB=c:\pub1\Distrib

rem Check if FileSystem Correct
echo Check AllUserProfile
if not exist "%ALLUSERSPROFILE%" exit /b 1

rem Check if Utilites Presents
echo Check if Utilities Presents
if not exist "%SystemRoot%\system32\find.exe" exit /b 1
if not exist "%UTIL%\nit.http.wise.wsf" exit /b 2
rem if not exist "%SystemRoot%\System32\bitsadmin.exe" exit /b 2
if not exist "%SystemRoot%\System32\wbem\WMIC.EXE" exit /b 1
if not exist "%SystemRoot%\System32\reg.exe" exit /b 1
if not exist "%SystemRoot%\System32\wscript.exe" exit /b 1
if not exist "%SystemRoot%\System32\cscript.exe" exit /b 1
if not exist "%SystemRoot%\System32\shutdown.exe" exit /b 1
if not exist "%SystemRoot%\System32\netsh.exe" exit /b 1
if not exist "%SystemRoot%\System32\attrib.exe" exit /b 1
rem if not exist "%TEMP%\nit.http.wise.wsf" exit /b 2
if not exist "%TEMP%" exit /b 1

rem Clean and Make Dowloaded Directory
rem
set PRODUCTNAMEDIR=%ALLUSERSPROFILE%\%FIRM_NAME%\%PRODUCT_NAME%

rem Check If System Product Directory Exists
rem
echo Check NIT Products Directory
set PRODUCTNAMEDIR=%ALLUSERSPROFILE%\%FIRM_NAME%\%PRODUCT_NAME%
if not exist "%PRODUCTNAMEDIR%" exit /b 1
if not exist "%UTIL%" exit /b 1
if not exist "%DEST_DIR%" exit /b 1
if not exist "%PUB1%" exit /b 1
if not exist "%TEMPPUB%" exit /b 1

rem Initialization Download Variables
rem
set http_pref001=http
set http_host001=file.tuneserv.ru
set http_port001=80
set http_dir0001=/WinUpdate/
set http_dir0000=/Exponenta/
set http_echodir=/WinUpdate/InitialCommon/

rem set CURL & WGET Variables
rem
echo Set CURL and WGET Variadles
set CURLEXE=%UTIL%\curl.exe
set WGETEXE=%UTIL%\wget.exe
if not exist "%CURLEXE%" echo %CURLEXE% not exist && exit /b 3
if not exist "%WGETEXE%" echo %WGETEXE% not exist && exit /b 4

rem Derivatives Variables
set hostecho=%http_pref001%://%http_host001%:%http_port001%%http_echodir%
set LocalFolder=%PRODUCTNAMEDIR%

rem set Path Variables
set CUR_FOLD=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set NEXT_FOLD=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/WinMF/

rem TEST Download and Execute echo.bat
rem
set ECHO=echo.bat
"%CURLEXE%" %hostecho%%ECHO% -o "%LocalFolder%\%ECHO%"
if not exist "%LocalFolder%\%ECHO%" echo %LocalFolder%\%ECHO% not exist && exit /b 6
rem call "%LocalFolder%\%ECHO%"

rem End Test

rem Download and Show Information Message
rem
set host01=%http_pref001%://%http_host001%:%http_port001%%CUR_FOLD%
set MESS=MESSWINXP-Framework.wsf
"%CURLEXE%" %host01%%MESS% -o "%LocalFolder%\%MESS%"
if not exist "%LocalFolder%\%MESS%" exit /b 6
"%SystemRoot%\System32\wscript.exe" "%LocalFolder%\%MESS%"

rem Download and Execute Payloads
rem 
set payloadHost=%http_pref001%://%http_host001%:%http_port001%/WinUpdate/WindowsMainUpdate/NET-Framework/
set payloadCommand=dotnetfx35.exe
if exist "%LocalFolder%\%payloadCommand%" del /Q /F "%LocalFolder%\%payloadCommand%"
%WGETEXE% %payloadHost%%payloadCommand% -O "%LocalFolder%\%payloadCommand%"
if not exist "%LocalFolder%\%payloadCommand%" echo "%LocalFolder%\%payloadCommand%" not Installed && goto Next_Script
"%LocalFolder%\%payloadCommand%" /s
set payloadCommand=dotNetFx40_Full_x86_x64.exe
if exist "%LocalFolder%\%payloadCommand%" del /Q /F "%LocalFolder%\%payloadCommand%"
%WGETEXE% %payloadHost%%payloadCommand% -O "%LocalFolder%\%payloadCommand%"
if not exist "%LocalFolder%\%payloadCommand%" echo "%LocalFolder%\%payloadCommand%" not Installed && goto Next_Script
"%LocalFolder%\%payloadCommand%" /s

rem End Payloads

:Next_Script
rem Download and Execute Next Script
rem
set host02=%http_pref001%://%http_host001%:%http_port001%%NEXT_FOLD%
set NEXT_BAT=NIT-SUDl-WinMF.cmd
SET REGISTRY_KEY_EX=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce
"%CURLEXE%" %host02%%NEXT_BAT% -o "%LocalFolder%\%NEXT_BAT%"
if not exist "%LocalFolder%\%NEXT_BAT%" exit /b 6
"%SystemRoot%\System32\reg.exe" ADD %REGISTRY_KEY_EX% /V "%NEXT_BAT%" /t REG_SZ /D "%SystemRoot%\System32\cmd.exe /c  \"%LocalFolder%\%NEXT_BAT%\"" /f

rem The End of the Script
:End
"%SystemRoot%\System32\shutdown.exe" /r /t 03
exit /b 0