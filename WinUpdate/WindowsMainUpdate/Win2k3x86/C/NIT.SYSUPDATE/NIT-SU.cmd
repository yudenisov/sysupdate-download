@echo off
rem *******************************************************
rem NIT-SU.cmd
rem This Script Checks Integrity of Computer Filesystem
rem and Install Necessary Modules
rem FOR WINDOWS 10X64
rem
rem PARAMETERS:	NONE
rem RETURNS:	NONE
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=Win2k3
set PRODUCT_NAME_FOLDER=Win2k3x86
SET FIRM_NAME=NIT
set OS_ARCH=x86

rem set ProductDir
set PRODUCTPROFILE=%ALLUSERSPROFILE%\%FIRM_NAME%\%PRODUCT_NAME%
md "%PRODUCTPROFILE%"

rem Check if NIT.SYSUPDATE Directory Exists
set NITSYS=C:\NIT.SYSUPDATE
set LocalFolder=%NITSYS%
if not exist "%LocalFolder%" goto EndError

rem Check Filesystem Integrity
echo Check Filesystem Integrity...
set CHCMD=NIT-Integrity.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
if errorlevel 3 goto EndError
if errorlevel 2 goto EndError
if errorlevel 1 goto EndError

rem Install NET Framework
echo Install NET Framework...
set CHCMD=NIT-SUDl-Framework.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
rem if errorlevel 6 goto EndError
rem if errorlevel 1 goto EndError

rem Install WinMF 2.0
echo Install WinMF 2.0...
set CHCMD=NIT-SUDl-WinMF.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
rem if errorlevel 6 goto EndError
rem if errorlevel 1 goto EndError

rem Install IE 8
echo Install IE 8...
set CHCMD=NIT-SUDl-IE8.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
rem if errorlevel 6 goto EndError
rem if errorlevel 1 goto EndError

rem Install SecUpdate 01
echo Install SecUpdate 01...
set CHCMD=NIT-SUDl-SecUpdate01.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
if errorlevel 7 goto EndError
rem if errorlevel 6 goto EndError
rem if errorlevel 1 goto EndError

rem Install Other
echo Install Other...
set CHCMD=NIT-SUDl-Other.cmd
if not exist "%LocalFolder%\%CHCMD%" goto EndError
call "%LocalFolder%\%CHCMD%"
rem if errorlevel 6 goto EndError
rem if errorlevel 1 goto EndError

goto End

:EndError
echo Error Runing Script

rem End Of Script
:End
echo The End of the Script
 