@echo off
rem *******************************************************
rem
rem NIT-SUDl-Framework.cmd
rem
rem This Scripts Check if Corresponded Version of
rem .NET Framework is Installed and Downloads and Installs
rem it if otherwise
rem
rem PARAMETERS: NO
rem RETURN:	0 if Success
rem		1 if Filesystem Error
rem		6 Wrong Server
rem
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=Win10
set PRODUCT_NAME_FOLDER=Win10x86
set FIRM_NAME=NIT
set OS_ARCH=x86

rem
rem Set Directories Path
set PATHCMD=%SystemRoot%\System32
set PATHCMDWOW=%SystemRoot%\SysWOW64
set UTIL=c:\Util
set NITSYS=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set AdminT=C:\Elevation
set TEMPPUB=c:\pub1\Distrib
set curdirforurl=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE

rem Check if FileSystem Correct
rem Clean and Make Dowloaded Directory
rem
set PRODUCTNAMEDIR=%ALLUSERSPROFILE%\%FIRM_NAME%\%PRODUCT_NAME%

rem Initialization Download Variables
rem
set http_pref001=http
set http_host001=file.tuneserv.ru
set http_port001=80
set http_dir0001=/WinUpdate/
set http_dir0000=/Exponenta/
set http_echodir=/WinUpdate/InitialCommon/

rem set CURL & WGET Variables
rem
echo Set CURL and WGET Variadles
set CURLEXE=%UTIL%\curl.exe
set WGETEXE=%UTIL%\wget.exe
rem Derivatives Variables
set hostecho=%http_pref001%://%http_host001%:%http_port001%%http_echodir%
set LocalFolder=%PRODUCTNAMEDIR%

rem set Path Variables
set CUR_FOLD=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Initial/
set NEXT_FOLD=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Other/

rem TEST Download and Execute echo.bat
rem
set ECHO=echo.bat
rem "%CURLEXE%" %hostecho%%ECHO% -o "%LocalFolder%\%ECHO%"
rem if not exist "%LocalFolder%\%ECHO%" echo %LocalFolder%\%ECHO% not exist && exit /b 6
rem call "%LocalFolder%\%ECHO%"
rem End Test

rem Set Variable Needed Reboot
rem
set TEMPREBOOT=0
set TEMPINSTALLED_NET35=1

rem Check if Any .NET Framework is Installed
rem
set REGKEY_NET35="HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5\1049"
rem For NET 4.0 - > 4.5
set REGKEY_NET4CLI="HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client"
set REGKEY_NET4FULL="HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full"

rem Check if .NET Dramework 3.5 Present
%PATHCMD%\reg.exe QUERY %REGKEY_NET35%
if ERRORLEVEL 1 goto IS_Net35_Install

rem Check If Dot NET Framework 3.5 is Installed
rem

%PATHCMD%\reg.exe QUERY %REGKEY_NET35% /v Install | %PATHCMD%\find.exe "0x1"
if ERRORLEVEL 1 set TEMPINSTALLED_NET35=0
if %TEMPINSTALLED_NET35%==1 goto Next_Script
rem End Check If Dot NET Framework 3.5 is Installed

rem Download and Execute Payloads
rem

:IS_Net35_Install
rem Download and Show Information Message
rem
set host01=%http_pref001%://%http_host001%:%http_port001%%CUR_FOLD%
set MESS=MESSWIN10-Framework.wsf
"%CURLEXE%" %host01%%MESS% -o "%LocalFolder%\%MESS%"
if not exist "%LocalFolder%\%MESS%" exit /b 6
"%SystemRoot%\System32\wscript.exe" "%LocalFolder%\%MESS%"

set payloadHost=%http_pref001%://%http_host001%:%http_port001%/WinUpdate/WindowsMainUpdate/NET-Framework/
set payloadCommand=dotnetfx35.exe
if exist "%LocalFolder%\%payloadCommand%" del /Q /F "%LocalFolder%\%payloadCommand%"
%WGETEXE% %payloadHost%%payloadCommand% -O "%LocalFolder%\%payloadCommand%"
if not exist "%LocalFolder%\%payloadCommand%" echo "%LocalFolder%\%payloadCommand%" not Installed && goto Next_Script
"%LocalFolder%\%payloadCommand%" /quiet /norestart
set TEMPREBOOT=1

rem End Payloads

:Next_Script
rem Download and Execute Next Script
rem
if %TEMPREBOOT%==0 goto End
echo Rebooting...
"%SystemRoot%\System32\shutdown.exe" /r /t 03

rem The End of the Script
:End
echo Script Done!
exit /b 0
