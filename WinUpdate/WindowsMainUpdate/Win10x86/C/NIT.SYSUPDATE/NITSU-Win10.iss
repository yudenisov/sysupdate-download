[Setup]
AppName=NITSU-Win10x86
AppVersion=1.0.0.0
AppCopyright=Copyright (c) New Internet Technology Inc.
DefaultDirName=C:\NIT.SYSUPDATE\
VersionInfoVersion=1.0.0.0
VersionInfoCompany=New Internet Tecnologies Inc.
VersionInfoCopyright=Copyright (c) New Internet Technology Inc.
SolidCompression=True
MinVersion=0,10.0
LanguageDetectionMethod=locale
VersionInfoProductName=NIT System Update for Windows 10/11 x86
VersionInfoProductVersion=1.0.0.0
OutputBaseFilename=SetupNITSU-Win10x86
AlwaysRestart=False
ArchitecturesAllowed=x86
SetupIconFile=.\NIT-Gear-logo.ico

[Files]
Source: "av-exclusions.html"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "nit.http.wise.wsf"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-Integrity.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SU.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-Framework.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SUDl-Other.cmd"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace
Source: "NIT-SU.wsf"; DestDir: "{app}"; Flags: recursesubdirs uninsremovereadonly uninsrestartdelete restartreplace

[Registry]
Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "NIT-SU"; ValueData: "C:\WINDOWS\System32\cscript.exe //NoLogo C:\NIT.SYSUPDATE\NIT-SU.wsf"; Flags: createvalueifdoesntexist uninsdeletevalue

