@echo off
rem *******************************************************
rem
rem NIT-SUDl-SecUpdate01.cmd
rem
rem This Templater file Downloads and Installs NIT
rem Program Products and Plugins
rem on local computer
rem
rem PARAMETERS: NO
rem RETURN:	0 if Success
rem		1 if Filesystem Error
rem		6 Wrong Server
rem
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=Win8.1
set PRODUCT_NAME_FOLDER=Win8.1x64
set FIRM_NAME=NIT
set OS_ARCH=x64

rem
rem Set Directories Path
set PATHCMD=%SystemRoot%\System32
set PATHCMDWOW=%SystemRoot%\SysWOW64
set UTIL=c:\Util
set NITSYS=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set AdminT=C:\Elevation
set TEMPPUB=c:\pub1\Distrib
set curdirforurl=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE

rem Check if FileSystem Correct
rem Clean and Make Dowloaded Directory
rem
set PRODUCTNAMEDIR=%ALLUSERSPROFILE%\%FIRM_NAME%\%PRODUCT_NAME%

rem Initialization Download Variables
rem
set http_pref001=http
set http_host001=file.tuneserv.ru
set http_port001=80
set http_dir0001=/WinUpdate/
set http_dir0000=/Exponenta/
set http_echodir=/WinUpdate/InitialCommon/

rem set CURL & WGET Variables
rem
echo Set CURL and WGET Variadles
set CURLEXE=%UTIL%\curl.exe
set WGETEXE=%UTIL%\wget.exe
rem Derivatives Variables
set hostecho=%http_pref001%://%http_host001%:%http_port001%%http_echodir%
set LocalFolder=%PRODUCTNAMEDIR%

rem set Path Variables
set CUR_FOLD=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/SecUpdate01/
set NEXT_FOLD=/WinUpdate/WindowsMainUpdate/%PRODUCT_NAME_FOLDER%/Other/

rem TEST Download and Execute echo.bat
rem
set ECHO=echo.bat
rem "%CURLEXE%" %hostecho%%ECHO% -o "%LocalFolder%\%ECHO%"
rem if not exist "%LocalFolder%\%ECHO%" echo %LocalFolder%\%ECHO% not exist && exit /b 6
rem call "%LocalFolder%\%ECHO%"
rem End Test

rem Set Variable Needed Reboot
rem
set TEMPREBOOT=0
set TEMPINSTALLED_UPDATE01=1

rem Check If UPDATE01 (KBXXXXX) is Installed
rem
set UPDATENUM=
rem %PATHCMD%\wbem\WMIC.exe qfe list | %PATHCMD%\find.exe "%UPDATENUM%"
rem if ERRORLEVEL 1 set TEMPINSTALLED_UPDATE01=0 ELSE set TEMPINSTALLED_UPDATE01=1
if %TEMPINSTALLED_UPDATE01%==1 goto IS_All_Installed
rem End Check If UPDATE01 (KBXXXXX) is Installed

rem Download and Execute Payloads
rem

rem Download and Show Information Message
rem
set host01=%http_pref001%://%http_host001%:%http_port001%%CUR_FOLD%
set MESS=MESSWIN8.1-SecUpdate01.wsf
"%CURLEXE%" %host01%%MESS% -o "%LocalFolder%\%MESS%"
if not exist "%LocalFolder%\%MESS%" exit /b 6
"%SystemRoot%\System32\wscript.exe" "%LocalFolder%\%MESS%"

set payloadHost=%http_pref001%://%http_host001%:%http_port001%%CUR_FOLD%

rem End Payloads

:IS_All_Installed
rem Download and Execute Next Script
rem
if %TEMPREBOOT%==0 goto End
echo Rebooting...
rem "%SystemRoot%\System32\shutdown.exe" /r /t 03

rem The End of the Script
:End
echo Script Done!
exit /b 0
